import pyautogui
import time
import os
import sys

x_coordinates = []
y_coordinates = []

refresh = sys.argv[1]

patrick = False
if (refresh == '-p'):
	patrick = True

def chrome_mac_refresh():
	pyautogui.keyDown('command')
	pyautogui.keyDown('shift')
	pyautogui.keyDown('r')
	pyautogui.keyUp('command')
	pyautogui.keyUp('shift')
	pyautogui.keyUp('r')

def qutebrowser_refresh():
	pyautogui.keyDown('r')
	pyautogui.keyUp('r')	

file_exists = os.path.exists('coordinates.txt')
if not file_exists:
	print("coordinates.txt does not exist or is not in the same file as this script.")
	exit(1)
with open('coordinates.txt') as f:
	for line in f.readlines():
		line = line.rstrip('\n')
		x_and_y = line.split(' ')
		x_coordinates.append(int(x_and_y[0]))
		y_coordinates.append(int(x_and_y[1]))

root_x = x_coordinates[-1]
root_y = y_coordinates[-1]

while True:
	for i in range(0, 4):
		time.sleep(.2)
		pyautogui.click(x_coordinates[0], y_coordinates[0])
		time.sleep(.2)
	for i in range (1, len(x_coordinates)):
		pyautogui.click(x_coordinates[i], y_coordinates[i])	
		pyautogui.click(root_x, root_y)
		
	if patrick:
		qutebrowser_refresh()
	else:
		chrome_mac_refresh()
	time.sleep(3)
