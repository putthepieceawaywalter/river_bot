# River Bot

this is a bot that clicks buttons really fast so a user can get a river permit at recreation.gov the moment they are released.

## Getting started

# run this command only the very first time you use this script:

sudo pip install -r requirements.txt

IT IS DANGEROUS TO USE SUDO WHEN COPYING CODE FROM AN UNTRUSTED SOURCE

# Tell script where you want to click
This script runs by trying to click a calendar date, then the book now button, the next calendar date, the book now button
and so on and so forth.

You need to tell the script where the buttons are

You do this by providing a file called coordinates.txt

run the current_mouse.py script with this command:

python3 current_mouse.py 

The terminal will display the current position of the mouse

Hover the mouse over the calendar dates you want to click
in a file called coordinates.txt fill in the x and y coordinates
on the very last line (and only on the very last line) put the coordinates for the book now position
There is an example text file called EXAMPLE_coordinates.txt included, you can look at it but it almost certainly will not work


# Running the script

python3 river_bot.py

If there are no errors it will allow you 2 seconds to minimize the terminal, then start clicking each of the provided calendar dates
when it gets to the end of the dates it will refresh the page and try again

